#Remoe Specific Droplet Snapshort backup


import requests
import datetime

#Please Replace Droplet_ID with you own Droplet-ID
droplet_id = 166687756

#Replace with your own Authorization Token
token = 'dop_v1_a9f3201229b8dff3761'

# Get a list of all snapshots for the droplet
url = f'https://api.digitalocean.com/v2/droplets/{droplet_id}/snapshots?include_destroyed=true'
headers = {'Authorization': f'Bearer {token}'}
response = requests.get(url, headers=headers)
snapshots = response.json()['snapshots']

# Delete snapshots that are older than 2 Days
now = datetime.datetime.now(datetime.timezone.utc)
for snapshot in snapshots:
    created_at = datetime.datetime.strptime(snapshot['created_at'], '%Y-%m-%dT%H:%M:%SZ').replace(tzinfo=datetime.timezone.utc)
    if (now - created_at).total_seconds() > 2 * 24 * 60 * 60:
        url = f'https://api.digitalocean.com/v2/snapshots/{snapshot["id"]}'
        response = requests.delete(url, headers=headers)
        if response.status_code == 204:
            print(f'Successfully deleted snapshot {snapshot["name"]}')
        else:
            print(f'Error deleting snapshot {snapshot["name"]}: {response.content}')
