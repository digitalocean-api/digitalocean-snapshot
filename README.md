# DigitalOcean Droplets Snapshot Backup via Api and Remove Snapshot X number of Time

1. You have to Createa a API Token from your Cloud Portal with Write Permission.
2. Get your API token from the DigitalOcean control panel. You can find the token in the "API" section of the left sidebar.
3. You could Setup [Doctl](https://gitlab.com/cka-certification/digitalocean_k8) for API Connectivity Testing.
4. Add Following python script and run via cronjobs from any Server.
5. After configure *Doctl* You could get your Droplet ID vai `doctl compute droplet list`

**Note: Replace Droplets ID and Api Token as you have**

```
############# Take Specific Droplet Snapshot Backup ############### 
import requests
import json
import datetime

#should be replace with your Droplet ID
droplet_id=349817662
##should be replace with your TOKEN
token='dop_v1_7e82be7f10d165531926f254b53af'

# Define the API endpoint and payload
endpoint = 'https://api.digitalocean.com/v2/droplets/{droplet_id}/actions'
payload = {
    'type': 'snapshot',
    'name': 'my-snapshot'
}

# Define the headers (including the Authorization header with the PAT)
headers = {
    'Content-Type': 'application/json',
    'Authorization': f'Bearer {token}'
}

# Send the POST request
response = requests.post(endpoint.format(droplet_id=droplet_id), headers=headers, data=json.dumps(payload))

# Check the status code of the response
if response.status_code == 201:
    print('Snapshot created successfully!')
else:
    print('Failed to create snapshot:', response.content)

########################### Delete Snapshot Older then X Number of Hours ##########################

# Define the API endpoint for listing snapshots and the droplet ID
endpoint = 'https://api.digitalocean.com/v2/snapshots'
#droplet_id = 123456789 # Replace with the ID of the droplet

# Define the headers (including the Authorization header with the PAT)
headers = {
    'Content-Type': 'application/json',
    'Authorization': f'Bearer {token}'
}

# Send the GET request to list the snapshots
response = requests.get(endpoint, headers=headers, params={'resource_type': 'droplet', 'resource_id': droplet_id})

# Check the status code of the response
if response.status_code == 200:
    snapshots = response.json()['snapshots']
    for snapshot in snapshots:
        snapshot_created_time = snapshot['created_at']
        snapshot_created_time = datetime.datetime.strptime(snapshot_created_time, '%Y-%m-%dT%H:%M:%SZ')
        time_difference = datetime.datetime.utcnow()- snapshot_created_time
        #Delete Droplets Snapshot Older then 32 Hours
        if time_difference > datetime.timedelta(hours=32):
            snapshot_id = snapshot['id']
            delete_endpoint = f'https://api.digitalocean.com/v2/snapshots/{snapshot_id}'
            delete_response = requests.delete(delete_endpoint, headers=headers)
            if delete_response.status_code == 204:
                print(f'Snapshot {snapshot_id} deleted successfully')
            else:
                print(f'Failed to delete snapshot {snapshot_id}:', delete_response.content)
else:
    print('Failed to list snapshots:', response.content)

```
In conclusion, the above-mentioned steps and commands can be used to retrieve DigitalOcean droplet IDs and account details. Whether you choose to use the DigitalOcean web console, API, or doctl, each method has its advantages and disadvantages, and it ultimately comes down to personal preference and the specific use case at hand. By understanding these methods, you can easily retrieve the necessary information to manage your DigitalOcean infrastructure efficiently.


